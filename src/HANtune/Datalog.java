/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import DAQList.DAQList;
import ErrorLogger.AppendToLogfile;
import HANtune.CustomFileChooser.FileType;
import can.DbcManager;
import datahandling.CurrentConfig;
import datahandling.Signal;
import util.MessagePane;
import util.Util;

/**
 * Performs the tasks related to the logging of incoming data
 *
 * @author Aart-Jan, Michiel Klifman
 */
public class Datalog {

    private final CurrentConfig currentConfig = CurrentConfig.getInstance();
    private final String fileExt = FileType.LOG_FILE.getExtension();
    private String fileName = "";
    private String prefix = "";
    private String error = "";
    private long startTime = 0;
    private boolean active = false;
    private final Map<String, Double> valuesBySignal = new LinkedHashMap<>();
    private static final String TIME_FORMAT = "0.00000";
    private static final String VALUE_FORMAT = "0.####################";
    private boolean newLineStarted = false;
    private boolean enabled = false;

    private FileOutputStream os = null;
    private PrintStream out = null;

    private static final Datalog instance = new Datalog();


    private Datalog() {

    }


    public static Datalog getInstance() {
        return instance;
    }


    public void setActive(boolean active) {
        this.active = active;
    }


    public boolean isActive() {
        return active;
    }


    public String getError() {
        return error;
    }


    public void setError(String error) {
        this.error = error;
    }


    public String getPrefix() {
        return prefix;
    }


    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }


    /**
     * Opens a new datalog file in the 'log' directory of HANtune. The filename is based on project name
     * plus the current date and time.
     *
     * @return
     */
    public boolean openDatalog() {
        HANtune hantune = HANtune.getInstance();
        String file_path = currentConfig.getLogFile_Path();
        try {
            fileName = constructLogfileName();
            File directory = new File(file_path);
            // if the directory does not exist, create it
            if (!directory.exists()) {
                directory.mkdirs();
            }
            String path = directory.getAbsolutePath() + "\\" + fileName;

            // open new file
            os = new FileOutputStream(path);
            out = new PrintStream(os);

            // add header information
            printHeader(hantune);

            printSignalProtocolNameLines();

            startTime = 0;
        } catch (FileNotFoundException e) {
            String errorString = "Unable to create output stream to filesystem. " + e.getMessage();
            setError(errorString);
            AppendToLogfile.appendError(Thread.currentThread(), e);
            closeDatalog();
            return false;
        }
        active = true;
        hantune.updateDatalogGuiComponents();
        return true;
    }


    /**
     * @param hantune
     */
    private void printHeader(HANtune hantune) {

        if (currentConfig.getProjectFile() != null) {
            out.println("Project: " + util.Util.getFilename(currentConfig.getProjectFile().getName()));
        } else {
            out.println("Project: No File");
        }

        String id = currentConfig.getXcpsettings().slaveid;
        if (id != null && !id.isEmpty()) {
            out.println("ECU: " + currentConfig.getXcpsettings().slaveid);
        }

        DbcManager.getAllDbcNames().forEach(s -> out.println("Active DBC: " + s));

        if (id != null && !id.isEmpty()) {
            float slaveFrequency = currentConfig.getXcpsettings().getSlaveFrequency();
            for (DAQList daqList : hantune.daqLists) {
                if (daqList.isActive()) {
                    out.println("DAQ List D"
                        + (daqList.getRelativeId() + 1) + ": " + daqList.getName() + ", " + util.Util
                            .getStringValue((double)daqList.getSampleFrequency(slaveFrequency), "0.##")
                        + "Hz");
                }
            }
        }

        out.println("Date: " + new Date());
        out.println(); // new line
    }


    /**
     *
     */
    private void printSignalProtocolNameLines() {

        // Print csv line containing protocol names: ;XCP;XCP;CAN;...
        out.print(";"); // empty cell
        // init arrays to store temporary values and print header of file
        List<Signal> signals = currentConfig.getAllReferencesOfType(Signal.class);
        valuesBySignal.clear(); // clear all entries from valuesBySignal to prevent previously loaded
                                // dbc signal values to show up
        for (Signal signal : signals) {
            // Name of signal protocol should appear in config.protocol name (CAN, XCP_ON_CAN, etc)
            // Convert all to upper case, just to be sure
            if (currentConfig.getProtocol().toString().toUpperCase()
                .indexOf(signal.getProtocolName().toUpperCase()) != -1) {
                valuesBySignal.put(signal.getName(), null);
                out.print(signal.getProtocolName() + ";");
            }
        }
        out.println();

        // Print csv line containing signal names: Time:;Signal_1;Signal_2;LedValue;...
        out.print("Time;");
        valuesBySignal.forEach((k, v) -> out.print(k + ";"));
        out.println("");
    }


    /**
     *
     */
    private String constructLogfileName() {
        LocalDateTime date = LocalDateTime.now();
        String strDate = date.format(DateTimeFormatter.ofPattern("yyyyMMdd_HHmmss"));
        if (prefix.equals("")) {
            prefix = util.Util.getFilename(currentConfig.getProjectFile().getName()).replace(".hml", "");
        }
        return prefix + strDate + "." + fileExt;
    }


    public void printTime(long timeStamp) {
        if (startTime == 0)
            startTime = timeStamp;

        double timeInSeconds = ((double)timeStamp - startTime) / 1000000;
        String time = Util.getStringValue(timeInSeconds, TIME_FORMAT) + ";";
        out.print(time);
        newLineStarted = true;
    }


    public void addValue(String name, double value) {
        if (newLineStarted) {
            valuesBySignal.replace(name, value);
        }
    }


    public void printValues() {
        if (newLineStarted && out != null) {
            valuesBySignal.forEach((k, v) -> {
                String value = v != null ? Util.getStringValue(v, VALUE_FORMAT) : "";
                out.print(value + ";");
                valuesBySignal.replace(k, null);
            });
            out.println();
            newLineStarted = false;
        }
    }


    public void printTimeAndValue(String name, double value, long timeStamp) {
        printTime(timeStamp);
        addValue(name, value);
        printValues();
    }


    /**
     * Closes the active datalog.
     */
    public void closeDatalog() {
        HANtune hantune = HANtune.getInstance();
        try {
            if (out != null) {
                out.close();
            }
            if (os != null) {
                os.close();
            }
            out = null;
            os = null;
        } catch (Exception e) {
            AppendToLogfile.appendError(Thread.currentThread(), e);
            setError(e.getMessage());
        }
        active = false;
        hantune.updateDatalogGuiComponents();
    }


    /**
     * Restarts datalog with new file, if possible
     */
    public void restart() {
        if (isLogging()) {
            closeDatalog();
        }
        if (currentConfig.getASAP2Data().getASAP2Measurements().size() > 0) {
            if (!openDatalog()) {
                MessagePane.showError("Could not start logfile:\r\n" + getError());
                closeDatalog();
            }
        }
    }


    /**
     * Returns whether a datalog file is currently active.
     *
     * @return
     */
    public boolean isLogging() {
        return (os != null && out != null);
    }


    /**
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }


    /**
     * @param fileName the fileName to set
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }


    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
