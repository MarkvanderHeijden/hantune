/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.actions;

import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import ASAP2.ASAP2Data;
import ASAP2.ASAP2Measurement;
import HANtune.CustomFileChooser;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtune;
import can.CanDataProcessor;
import can.DbcManager;
import datahandling.CurrentConfig;
import nl.han.hantune.datahandling.LogReader.LogDataReaderCsv;
import nl.han.hantune.datahandling.LogReader.LogDataReaderCsv.ReaderResult;
import nl.han.hantune.datahandling.LogReader.LogDataReaderCsv.ResultItem;
import nl.han.hantune.gui.dialogs.LogPlayerDialog;
import util.CursorWait;
import util.MessagePane;

import static java.awt.event.InputEvent.SHIFT_DOWN_MASK;

@SuppressWarnings("serial")
public class LogReaderAction extends LogPlayerDialog {
    private static final long MILLIS_IN_SEC = 1000L;
    private static final long MIN_DELAY_MILLIS = 4L; // limit max read speed per line at 250 Hz
    private static final String LOG_TIME_FORMAT = "#0.000";
    private static final int MIN_NUMBER_OF_PAGES = 10;
    private static final int DEFAULT_ITEMS_PER_PAGE = 100;
    private static final int MIN_ITEMS_PER_PAGE = 5;
    private static final int MIN_FEED_ITEMS = 3;
    private static final int DEFAULT_SPEED_FACTOR = 1;
    private static final int INIT_FAST_SPEED_FACTOR = 5;
    private static final int SEC_TO_MICROS = 1000000;
    private static final DecimalFormat df = new DecimalFormat(LOG_TIME_FORMAT);

    private static LogReaderAction logReaderAction = null;
    private boolean active = false;
    HANtune hanTune;
    private CurrentConfig currentConfig;

    private File logFile;
    private LogDataReaderCsv reader;
    private boolean isInitComplete;

    private Timer timer = new Timer(true);
    private boolean isPlaying = false;
    private boolean isPlayingForward = true;
    private int speedFactor = DEFAULT_SPEED_FACTOR;
    private int pageSize = DEFAULT_ITEMS_PER_PAGE;


    private LogReaderAction() {
        super();

        this.hanTune = HANtune.getInstance();
        this.setModal(false);
        this.setAlwaysOnTop(false);

        currentConfig = CurrentConfig.getInstance();
        df.setRoundingMode(RoundingMode.HALF_DOWN);
    }


    public static LogReaderAction getInstance() {
        // singleton class
        if (logReaderAction == null) {
            logReaderAction = new LogReaderAction();
        }
        return logReaderAction;
    }


    /**
     * Request for filename and execute logfile player
     */
    public void chooseLogFileAndRead() {
        isInitComplete = false;
        // dispose of possible previous log reader
        logReaderAction.dispose();

        CustomFileChooser.FileType fType = FileType.LOG_FILE;

        CustomFileChooser fileChooser = new CustomFileChooser("Open log file for reading", fType);

        File file = fileChooser.chooseOpenDialog(hanTune, "Select");
        if (file != null) {
            if (!isContinueDisconnectOk(file.getName())) {
                return;
            }

            logFile = file;
            startInitReadLogfileDialog();
        }
    }

    private boolean isContinueDisconnectOk(String fName) {
        if (hanTune.isConnected()) {
            if (MessagePane.showConfirmDialog(hanTune, "HANtune is currently connected to the target.\n "
                + "Connection will be closed during Read log operation.\n"
                + " Disconnect and continue?", "Continue Read Log " + fName + "?") == JOptionPane.OK_OPTION) {
                hanTune.connectProtocol(false);
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    private void startInitReadLogfileDialog() {
        setLocationRelativeTo(hanTune);
        setTitle("Initializing " + logFile.getName() + ". Please wait...");
        CursorWait.startWaitCursor(hanTune.getRootPane());

        setVisible(true);
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void dispose() {
        active = false;
        stopPlaying();

        super.dispose();
    }

    @Override
    protected boolean handleInitComplete() {
        if (isInitComplete) {
            return true;
        }
        isInitComplete = true;

        CursorWait.stopWaitCursor(hanTune.getRootPane());

        reader = new LogDataReaderCsv();
        if (!reader.init(logFile)) {
            handleReaderException();
            return false;
        }

        if (!isContinueLogIdsOk(logFile.getName())) {
            dispose();
            return false;
        }

        setTitle("Read log " + logFile.getName());
        setTotalTimeLabel("Time: " + df.format(reader.getMaximumLogTimeSec()));
        setTotalLinesLabel("Lines: " + reader.getNumDataLines());
        pageSize = calculatePageSize();
        handleGotoStartLogging();

        activateRootComponent();

        active = true;

        return true;
    }


    private boolean isContinueLogIdsOk(String fName) {
        String err = "";
        if (reader.getLogHeaderInfo().getEcuId() != null) {
            if (currentConfig.getASAP2Data() == null || currentConfig.getASAP2Data().getEpromID() == null) {
                err += " - No ASAP2 file active. Cannot display ASAP2 signals.\n";
            } else if (!reader.getLogHeaderInfo().getEcuId().equals(currentConfig.getASAP2Data().getEpromID())) {
                err += " - Current ASAP2 application ID does not match the application ID from this log file:\n"
                    + "        ASAP2 ID: " + currentConfig.getASAP2Data().getEpromID() + "\n"
                    + "        Log ID:   " + reader.getLogHeaderInfo().getEcuId() + "\n"
                    + "    Not all values might be displayed properly while reading this log.\n";
            }
        }
        if (reader.getLogHeaderInfo().getDbcInfos() != null) {
            if (DbcManager.getDbcList().isEmpty()) {
                err += " - No DBC file active. Cannot display DBC messages.\n";
            } else {
                for (String info : reader.getLogHeaderInfo().getDbcInfos()) {
                    err = addWarning(err, info);
                }
            }
        }
        if (!err.isEmpty()) {
            return MessagePane.showConfirmDialog(hanTune, "Warning: \n" + err + "\nContinue anyway?",
                "Continue Read Log " + fName + "?") == JOptionPane.OK_OPTION;
        }
        return true;
    }


    private String addWarning(String err, String info) {
        if (DbcManager.getDbcByName(info) == null) {
            err += " - DBC from this log file does not match any active DBC file.\n"
                + "   Not all CAN values might be displayed properly while reading this log.\n"
                + "   Log DBC name: " + info + "\n";
        }
        return err;
    }

    @Override
    protected void txtCurrentTimeFocusGained(FocusEvent evt) {
        stopPlaying();
    }

    @Override
    protected boolean handleGotoSliderPos(int val) {
        stopPlaying();
        try {
            reader.getRelativeElement((double) val / MAX_SLIDER_VALUE);
        } catch (IOException e) {
            handleReaderException();
        }
        // ... and wind all lines into view
        updateDisplay(readLogItemsAtCurrentReadingLocation(pageSize, true));

        return true;
    }


    @Override
    protected boolean handleGotoStartLogging() {
        stopPlaying();
        try {
            List<LogDataReaderCsv.ReaderResult> readerResults = new ArrayList<>();

            readerResults.add(reader.getFirstElement()); // move current reading location to start of file
            for (int count = 1; count < MIN_FEED_ITEMS; count++) {
                readerResults.add(reader.getNextElement());
            }

            updateDisplay(readerResults);
        } catch (IOException e) {
            handleReaderException();
        }
        return false;
    }


    @Override
    protected boolean handleGotoEndLogging() {
        stopPlaying();
        try {
            reader.getLastElement(); // move current reading location to end of file
            int moveSize = reader.moveCurrentReadingLocation(pageSize, false); // move 1 page backward

            List<LogDataReaderCsv.ReaderResult> readerResults = new ArrayList<>();
            while (moveSize-- > 0) {
                readerResults.add(reader.getNextElement());
            }

            updateDisplay(readerResults);
        } catch (IOException e) {
            handleReaderException();
        }
        return false;
    }


    @Override
    protected boolean handleStepBackward() {
        stopPlaying();
        updateDisplay(readLogItemsAtCurrentReadingLocation(1, false));

        return true;
    }


    @Override
    protected boolean handleStepForward() {
        stopPlaying();
        updateDisplay(readLogItemsAtCurrentReadingLocation(1, true));

        return true;
    }

    @Override
    protected boolean handlePageDownLogging() {
        stopPlaying();
        updateDisplay(readLogItemsAtCurrentReadingLocation(pageSize, false));

        return true;
    }

    @Override
    protected boolean handlePageUpLogging() {
        stopPlaying();
        updateDisplay(readLogItemsAtCurrentReadingLocation(pageSize, true));

        return true;
    }

    private int calculatePageSize() {
        int size = DEFAULT_ITEMS_PER_PAGE;
        if ((reader.getNumDataLines() < MIN_NUMBER_OF_PAGES * DEFAULT_ITEMS_PER_PAGE) &&
            ((size = reader.getNumDataLines() / MIN_NUMBER_OF_PAGES) < MIN_ITEMS_PER_PAGE)) {
            // try to divide small logfiles in roughly MIN_NUMBER_OF_PAGES
            size = MIN_ITEMS_PER_PAGE;
        }
        return size;
    }


    // read numItems from logging data. Start reading at current reading location
    private List<ReaderResult> readLogItemsAtCurrentReadingLocation(int numItems, boolean forwardDirection) {
        List<ReaderResult> rdrRslts = new ArrayList<>();
        try {
            if (forwardDirection) {
                for (int iCnt = 0; iCnt < numItems; iCnt++) {
                    rdrRslts.add(reader.getNextElement());
                }
            } else {
                // Always show data from left to right in scope windows. Therefore move readingLocation
                // 1 page + numItems to the 'left'. Followed by displaying numItems of adjacent elements
                int feedSize = reader.moveCurrentReadingLocation(numItems + pageSize, false) - numItems;
                for (int iCnt = 0; iCnt < feedSize; iCnt++) {
                    rdrRslts.add(reader.getNextElement());
                }
            }
        } catch (IOException e) {
            handleReaderException();
        }
        return rdrRslts;
    }


    /**
     * newPos string holding position in seconds ("xx.yyy")
     */
    @Override
    protected boolean handlePositionLogging(String newPos) {
        stopPlaying();
        // get a page BEFORE newPos
        int lines = reader.gotoRefElementWithOffset(newPos, pageSize, false);
        if (lines <= 0) {
            activateRootComponent();
            return false;
        }
        // ... and wind all lines into view
        updateDisplay(readLogItemsAtCurrentReadingLocation(lines, true));
        activateRootComponent();

        return true;
    }


    @Override
    protected boolean handleFormWindowLostFocus() {
        return true;
    }

    @Override
    protected boolean handleFastForwardBtn() {
        if (!isPlaying) {
            isPlayingForward = true;
            speedFactor = INIT_FAST_SPEED_FACTOR;
            startPlaying();
            executeRunTask();
        } else {
            if (isPlayingForward) {
                speedFactor = incrementSpeedFactor(speedFactor);
            }
            isPlayingForward = true;
            setPlayingState(true, speedFactor, true);
        }

        return true;
    }


    @Override
    protected boolean handleFastRewindBtn() {
        if (!isPlaying) {
            isPlayingForward = false;
            speedFactor = INIT_FAST_SPEED_FACTOR;
            startPlaying();
            executeRunTask();
        } else {
            if (!isPlayingForward) {
                speedFactor = incrementSpeedFactor(speedFactor);
            }
            isPlayingForward = false;
            setPlayingState(true, speedFactor, false);
        }

        return true;
    }


    @Override
    protected void logPlayerKeyPressed(java.awt.event.KeyEvent evt) {
        switch (evt.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if ((evt.getModifiersEx() & SHIFT_DOWN_MASK) != 0) {
                    handlePageDownLogging();
                } else {
                    handleStepBackward();
                }
                activateButtonPrev();
                break;

            case KeyEvent.VK_RIGHT:
                if ((evt.getModifiersEx() & SHIFT_DOWN_MASK) != 0) {
                    handlePageUpLogging();
                } else {
                    handleStepForward();
                }
                activateButtonNext();
                break;

            case KeyEvent.VK_PAGE_DOWN:
                handlePageDownLogging();
                activateButtonPrev();
                break;

            case KeyEvent.VK_PAGE_UP:
                handlePageUpLogging();
                activateButtonNext();
                break;

            case KeyEvent.VK_P:
                handlePlayPauseBtn();
                activatePlayPauseBtn();
                break;

            case KeyEvent.VK_F:
                handleFastForwardBtn();
                activateFastForwardBtn();
                break;

            case KeyEvent.VK_G:
                stopPlaying();
                activateTxtCurrentTime();
                break;

            case KeyEvent.VK_R:
                handleFastRewindBtn();
                activateRewindBtn();
                break;

            case KeyEvent.VK_HOME:
            case KeyEvent.VK_B:
                handleGotoStartLogging();
                activateBeginningBtn();
                break;

            case KeyEvent.VK_END:
            case KeyEvent.VK_E:
                handleGotoEndLogging();
                activateEndBtn();
                break;

            case KeyEvent.VK_SPACE:
                if (evt.getComponent().getName().equals(SLIDER_TIME_POS)) {
                    stopPlaying();
                }
                break;

            default:
                break;
        }
    }

    @Override
    protected void formWindowClosed(WindowEvent evt) {
        reader.terminateReader();
    }


    private int incrementSpeedFactor(int spd) {
        switch (spd) {
            case 0:
            case 1:
                return 5;

            case 5:
                return 10;

            case 10:
                return 20;

            default:
                return 1;
        }
    }


    private static TimerTask timerWrap(Runnable r) {
        return new TimerTask() {
            @Override
            public void run() {
                r.run();
            }
        };
    }


    private void stopPlaying() {
        timer.cancel();
        isPlaying = false;
        speedFactor = DEFAULT_SPEED_FACTOR;
        setPlayingState(false, speedFactor, isPlayingForward);
    }


    private void startPlaying() {
        isPlaying = true;
        setPlayingState(true, speedFactor, isPlayingForward);
        timer.cancel();
        timer = new Timer(true);
    }


    private void executeRunTask() {
        if (isPlaying) {
            if (!updateDisplay(readLogItemsAtCurrentReadingLocation(1, isPlayingForward))) {
                stopPlaying();
                return;
            }


            timer.schedule(timerWrap(this::executeRunTask), calculateNewDelay());
        }
    }


    private long calculateNewDelay() {
        long delay = (long) (reader.getDelayToAdjacentStep(isPlayingForward) * MILLIS_IN_SEC) / speedFactor;

        return delay < MIN_DELAY_MILLIS ? MIN_DELAY_MILLIS : delay;
    }


    @Override
    protected boolean handlePlayPauseBtn() {
        if (isPlaying) {
            stopPlaying();
        } else {
            speedFactor = DEFAULT_SPEED_FACTOR;
            isPlayingForward = true;
            startPlaying();
            executeRunTask();
        }
        return true;
    }


    private boolean updateDisplay(List<ReaderResult> rdrResults) {
        if (rdrResults.isEmpty()) {
            return false;
        }

        // update all elements of rdrResults to layout (necessary for Scope panels)
        SwingUtilities.invokeLater(() -> rdrResults.forEach(this::updateLayout));

        // update rdrResults for dialog
        updateDialog(rdrResults);

        return true;
    }


    private void handleReaderException() {
        CursorWait.stopWaitCursor(this.getRootPane());

        MessagePane.showError("Error reading logfile. Please select another file.\n" + reader.getError());
        dispose();
    }


    private void updateDialog(List<ReaderResult> rdrRslts) {
        ReaderResult rslt = rdrRslts.get(rdrRslts.size() - 1);
        if (rslt == null) {
            // search backwards if last element doesn't contain data
            int idx = rdrRslts.size() - 1;
            while (--idx > 0) {
                if ((rslt = rdrRslts.get(idx)) != null) {
                    break;
                }
            }
            if (rslt == null) {
                return;
            }
        }

        setCurrentTimeTxtField(df.format(rslt.getTimeStampSecs()));

        int pos = (int) (((rslt.getTimeStampSecs() - reader.getMinimumLogTimeSec()) * MAX_SLIDER_VALUE)
            / (reader.getMaximumLogTimeSec() - reader.getMinimumLogTimeSec()));
        setSliderPosition(pos);
    }


    private void updateLayout(ReaderResult rdrRslt) {
        if (rdrRslt == null) {
            return;
        }

        long timeVal = (long) (rdrRslt.getTimeStampSecs() * SEC_TO_MICROS);
        ASAP2Data asap2Data = currentConfig.getASAP2Data();

        for (ResultItem item : rdrRslt.getResultItems()) {
            ASAP2Measurement mmt;
            if (item.getProtocolName().equals("XCP")) {
                if (asap2Data != null && (mmt = asap2Data.getMeasurementByName(item.getItemName())) != null) {
                    mmt.setValueAndTime(item.getItemValue(), timeVal);
                }
            } else if (item.getProtocolName().equals("CAN")) {
                CanDataProcessor.processCanMessage(item.getItemName(), item.getItemValue(), timeVal);
            }
        }
    }
}
