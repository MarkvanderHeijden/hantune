/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.guiComponents.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.prefs.Preferences;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;

import HANtune.CommunicationSettings;
import HANtune.Datalog;
import HANtune.HANtune;
import HANtune.UserPreferences;

@SuppressWarnings("serial")
public class CommunicationMenu extends JMenu {
    private HANtune hanTune;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);
    
    private MenuItem xcpConnectMenuItem;
    private MenuItem xcpSettingsMenuItem;
    private JCheckBoxMenuItem xcpDatalogCheckboxMenuItem;
    private MenuItem xcpDatalogFilenameMenuItem;

    public CommunicationMenu(HANtune hantune) {
        this.hanTune = hantune;
        setText("Communication");
        setMnemonic('C');
        
        xcpConnectMenuItem = new MenuItem.Builder("Connect")
            .acceleratorKey(KeyEvent.VK_F5, 0)
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        xcpConnectMenuItemActionPerformed(evt);
                    }
                })
            .build();
        add(xcpConnectMenuItem);
        
        xcpSettingsMenuItem = new MenuItem.Builder("Communication Settings")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        xcpSettingsMenuItemActionPerformed(evt);
                    }
                })
            .build();
        add(xcpSettingsMenuItem);

        xcpDatalogCheckboxMenuItem = new javax.swing.JCheckBoxMenuItem(); // No builder available
        xcpDatalogCheckboxMenuItem.setText("Enable Datalogging");
        xcpDatalogCheckboxMenuItem.addActionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        xcpDatalogCheckboxMenuItemActionPerformed(evt);
                    }
                });
        add(xcpDatalogCheckboxMenuItem);

        xcpDatalogFilenameMenuItem = new MenuItem.Builder("Modify Datalog Filename")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        xcpDatalogFilenameMenuItemActionPerformed(evt);
                    }
                })
            .build();
        add(xcpDatalogFilenameMenuItem);
    }
    

    public void updateItemsDataLogging() {
        boolean enableLogger = userPrefs.getBoolean("rememberLoggerState", false)
            && userPrefs.getBoolean("loggerEnabled", false);
        xcpDatalogCheckboxMenuItem.setSelected(enableLogger);
    }
    
    boolean dataLoggingIsSelected() {
        return xcpDatalogCheckboxMenuItem.isSelected();
    }
    
    
    void dataLoggingSetSelected(boolean select) {
        xcpDatalogCheckboxMenuItem.setSelected(select);
    }
    
    
    void communicConnectSetText(String newText) {
        xcpConnectMenuItem.setText(newText);
    }
    
    
    private void xcpConnectMenuItemActionPerformed(ActionEvent evt) {
        // toggle currentConfig.getXcp() connection on/off
        if (!hanTune.isConnected()) {
            hanTune.connectProtocol(true);
        } else {
            hanTune.connectProtocol(false);
        }
    }

    private void xcpSettingsMenuItemActionPerformed(ActionEvent evt) {
        if (hanTune.getCommunicationSettings() != null
            && hanTune.getCommunicationSettings().isDisplayable()) {
            hanTune.getCommunicationSettings().toFront();
        } else {
            hanTune.setCommunicationSettings(new CommunicationSettings());
            hanTune.getCommunicationSettings().setVisible(true);
        }
    }


    private void xcpDatalogCheckboxMenuItemActionPerformed(ActionEvent evt) {
        // menu item has just received new state here
        hanTune.toggleDatalog(xcpDatalogCheckboxMenuItem.isSelected());
    }


    private void xcpDatalogFilenameMenuItemActionPerformed(ActionEvent evt) {
        Datalog datalog = Datalog.getInstance();
        String name =
            HANtune.showQuestion("Datalog Filename", "Datalog Filename Prefix:", datalog.getPrefix());
        if (name != null) {
            datalog.setPrefix(name);
        }
    }

}
