/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package HANtune.guiComponents.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JPopupMenu.Separator;

import HANtune.CustomFileChooser;
import HANtune.CustomFileChooser.FileType;
import HANtune.HANtune;
import HANtune.UserPreferences;
import HANtune.actions.CreateCalibration;
import HANtune.actions.FlashCalibration;
import HANtune.actions.LogReaderAction;
import datahandling.CurrentConfig;
import util.MessagePane;

@SuppressWarnings({"serial"})
public class FileMenu extends JMenu {
    private HANtune hanTune;

    private MenuItem newProjectMenuItem;
    private MenuItem openProjectMenuItem;
    private MenuItem closeProjectMenuItem;
    private MenuItem saveMenuItem;
    private MenuItem saveAsMenuItem;
    private JMenu newSubMenu;
    public JMenu recentProjectsSubMenu;
    private MenuItem newDaqlistMenuItem;
    private MenuItem newLayoutMenuItem;
    private MenuItem newCalibrationMenuItem;
    private JMenu importSubMenu;
    private MenuItem importASAP2MenuItem;
    private MenuItem importDbcMenuItem;
    private MenuItem importDataMenuItem;
    private JMenu exportSubMenu;
    private MenuItem exportServiceProjectMenuItem;
    private Separator itemSeparator1;
    private Separator itemSeparator2;
    private Separator itemSeparator3;
    private Separator itemSeparator4;
    private MenuItem flashTargetMenuItem;
    private MenuItem playLogFileMenuItem;
    private MenuItem exitMenuItem;
    private static final int RECENT_PROJECTS_LIST_SIZE = 15;
    private Preferences userPrefs = Preferences.userNodeForPackage(UserPreferences.class);


    public FileMenu(HANtune hantune) {
        this.hanTune = hantune;
        setText("File");
        setMnemonic('F');

        newProjectMenuItem =
            new MenuItem.Builder("New Project")
                .acceleratorKey(KeyEvent.VK_N, InputEvent.CTRL_MASK)
                .icon("/images/tree/PROJECT.png")
                .actionListener(
                    new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            newProjectMenuItemActionPerformed(evt);
                        }
                    })
                .build();
        add(newProjectMenuItem);

        add(itemSeparator1 = new JPopupMenu.Separator());

        openProjectMenuItem = new MenuItem.Builder("Open Project...")
            .acceleratorKey(KeyEvent.VK_O, InputEvent.CTRL_MASK)
            .icon("/images/open.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        openMenuItemActionPerformed(evt);
                    }
                })
            .build();
        add(openProjectMenuItem);


        recentProjectsSubMenu = new JMenu("Recent Projects");
        add(recentProjectsSubMenu);
        recentProjectsSubMenu.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent me) {
                rebuildRecentProjectsMenu(userPrefs);
            }
        });
        add(recentProjectsSubMenu);

        closeProjectMenuItem = new MenuItem.Builder("Close Project")
            .actionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    closeMenuItemActionPerformed(evt);
                }
            })
            .build();
        add(closeProjectMenuItem);

        saveMenuItem =
            new MenuItem.Builder("Save Project")
                .acceleratorKey(KeyEvent.VK_S, InputEvent.CTRL_MASK)
                .icon("/images/save.png")
                .actionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        saveMenuItemActionPerformed(evt);
                    }
                })
                .build();
        add(saveMenuItem);


        saveAsMenuItem = new MenuItem.Builder("Save Project As...")
            .actionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    saveAsMenuItemActionPerformed(evt);
                }
            })
            .build();
        add(saveAsMenuItem);


        add(itemSeparator2 = new JPopupMenu.Separator());

        // Submenu: New
        newSubMenu = new JMenu();
        newSubMenu.setText("New");

        newDaqlistMenuItem = new MenuItem.Builder("New DAQ list")
            .icon("/images/tree/DAQLIST.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        newDaqlistMenuItemActionPerformed(evt);
                    }
                })
            .build();
        newSubMenu.add(newDaqlistMenuItem);

        newLayoutMenuItem = new MenuItem.Builder("New Layout")
            .icon("/images/tree/LAYOUT.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        newLayoutMenuItemActionPerformed(evt);
                    }
                })
            .build();
        newSubMenu.add(newLayoutMenuItem);


        newCalibrationMenuItem = new MenuItem.Builder("New Calibration")
            .icon("/images/tree/CALIBRATION.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        newCalibrationMenuItemActionPerformed(evt);
                    }
                })
            .build();
        newSubMenu.add(newCalibrationMenuItem);
        add(newSubMenu); // add New submenu to file menu

        // Submenu: Import
        importSubMenu = new JMenu();
        importSubMenu.setText("Import");


        importASAP2MenuItem = new MenuItem.Builder("ASAP2 File")
            .icon("/images/tree/ASAP2.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        importASAP2MenuItemActionPerformed(evt);
                    }
                })
            .build();
        importSubMenu.add(importASAP2MenuItem);

        importDbcMenuItem = new MenuItem.Builder("DBC")
            .icon("/images/sidepanel/dbc.png")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        importDbcActionPerformed(evt);
                    }
                })
            .build();
        importSubMenu.add(importDbcMenuItem);

        importDataMenuItem = new MenuItem.Builder("Import Data")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        importDataMenuItemActionPerformed(evt);
                    }
                })
            .build();
        importSubMenu.add(importDataMenuItem);
        add(importSubMenu); // add Import submenu to file menu

        // Submenu: Export
        exportSubMenu = new JMenu();
        exportSubMenu.setText("Export");

        exportServiceProjectMenuItem = new MenuItem.Builder("Service tool Project...")
            .toolTiptext("Export current project as Service Tool project file")
            .actionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    exportServiceProjectMenuItemActionPerformed(evt);
                }
            })
            .build();
        exportSubMenu.add(exportServiceProjectMenuItem);
        add(exportSubMenu); // add Export submenu to file menu

        add(itemSeparator3 = new JPopupMenu.Separator());

        flashTargetMenuItem = new MenuItem.Builder("Flash Target...")
            .toolTiptext("Select a file to flash directly into target")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        flashTargetActionPerformed(evt);
                    }
                })
            .build();
        add(flashTargetMenuItem);

        playLogFileMenuItem = new MenuItem.Builder("Read log file...")
            .toolTiptext("Select a logfile to examine")
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        readLogActionPerformed(evt);
                    }
                })
            .build();
        add(playLogFileMenuItem);

        add(itemSeparator4 = new JPopupMenu.Separator());

        exitMenuItem = new MenuItem.Builder("Exit")
            .acceleratorKey(KeyEvent.VK_F4, InputEvent.ALT_MASK)
            .actionListener(
                new ActionListener() {
                    public void actionPerformed(ActionEvent evt) {
                        exitMenuItemActionPerformed(evt);
                    }
                })
            .build();
        add(exitMenuItem);
    }

    /*
     * Reconstructs the Recent Projects menu under the File Menus,
     * based on the "lastProjectPathList" string saved in userPreferences
     */
    public JMenu rebuildRecentProjectsMenu(Preferences userPrefs) {
        recentProjectsSubMenu.removeAll();
        MenuItem menuItem = null;
        List<String> projectPaths = CurrentConfig.getInstance().getHANtuneManager().getLastProjectPathList(userPrefs);

        for (String path : projectPaths) {                                      // add an actionListener to each menuItem with a reference to the project to be loaded
            menuItem = new MenuItem.Builder(path)
                .actionListener(
                    new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent evt) {
                            menuItemActionPerformed(path);
                        }
                    })
                .build();
            recentProjectsSubMenu.add(menuItem, 0);                              // add new menuItem to the top of the list
        }

        if (menuItem ==
            null) {                                                 // give a sensible hint towards the user that there is no recent project yet
            menuItem = new MenuItem.Builder("empty...").build();
            menuItem.setEnabled(false);
            recentProjectsSubMenu.add(menuItem);
        }
        return recentProjectsSubMenu;
    }

    private void menuItemActionPerformed(String project) {
        CurrentConfig currentConfig = CurrentConfig.getInstance();
        File projectFile = new File(project);

        currentConfig.getHANtuneManager().closeProject();
        if (!currentConfig.getHANtuneManager().openProject(projectFile, false)) {
            MessagePane.showError("Error: " + currentConfig.getHANtuneManager().getError()
                + "\nCould not open file: " + projectFile.getName());
            HANtune.getInstance().getHanTuneProject().requestNewProject();
        }
    }

    public void updateItemsServiceModeVisibility() {
        boolean isNormalMode = !CurrentConfig.getInstance().isServiceToolMode();

        // Set only service tool related menuItems
        newProjectMenuItem.setVisible(isNormalMode);
        itemSeparator1.setVisible(isNormalMode);

        closeProjectMenuItem.setVisible(isNormalMode);
        saveMenuItem.setVisible(isNormalMode);
        saveAsMenuItem.setVisible(isNormalMode);
        exportServiceProjectMenuItem.setVisible(isNormalMode);
        itemSeparator2.setVisible(isNormalMode);

        newSubMenu.setVisible(isNormalMode);
        importSubMenu.setVisible(isNormalMode);
        exportSubMenu.setVisible(isNormalMode);
        itemSeparator3.setVisible(isNormalMode);

        itemSeparator4.setVisible(isNormalMode);

        flashTargetMenuItem.setVisible(isNormalMode);
        playLogFileMenuItem.setVisible(isNormalMode);
    }

    private void newProjectMenuItemActionPerformed(ActionEvent evt) {
        hanTune.getHanTuneProject().requestNewProject();
    }


    private void openMenuItemActionPerformed(ActionEvent evt) {
        hanTune.getHanTuneProject().requestOpenProject();
    }


    private void closeMenuItemActionPerformed(ActionEvent evt) {
        hanTune.getHanTuneProject().requestCloseProject();
    }


    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        if (hanTune.currentConfig.getProjectFile() != null) {
            hanTune.getHanTuneProject().requestSaveProject(false, true);
        } else {
            hanTune.getHanTuneProject().requestSaveAsProject();
        }
    }


    private void saveAsMenuItemActionPerformed(ActionEvent evt) {
        hanTune.getHanTuneProject().requestSaveAsProject();
    }


    private void exportServiceProjectMenuItemActionPerformed(ActionEvent evt) {
        hanTune.getHanTuneProject().requestSaveAsServiceProject();
    }


    private void newDaqlistMenuItemActionPerformed(ActionEvent evt) {
        createNewDAQlist();
    }


    private void newLayoutMenuItemActionPerformed(ActionEvent evt) {
        String name = HANtune.showQuestion("New layout", "Name:", "");
        if (name == null) {
            return; // cancel has been pressed.
        }

        CurrentConfig.getInstance().createNewLayout(name);
    }

    private void newCalibrationMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        CreateCalibration createCalib = new CreateCalibration(hanTune);
        createCalib.createNewCalibration();
    }

    private void importASAP2MenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        hanTune.requestOpenASAP2();
    }


    private void importDbcActionPerformed(java.awt.event.ActionEvent evt) {
        CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.DBC_FILE);
        File dbcFile = fileChooser.chooseOpenDialog(this, "Import");

        if (dbcFile != null) {
            hanTune.currentConfig.getHANtuneManager().newDbc(dbcFile);
            hanTune.showProjectPanel(true);
        }
    }


    private void importDataMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        int option = hanTune.getHanTuneProject().requestSaveProject(true, false);
        if (option != JOptionPane.CLOSED_OPTION && option != JOptionPane.CANCEL_OPTION) {
            CustomFileChooser fileChooser = new CustomFileChooser("Import", FileType.PROJECT_FILE);
            File projectFile = fileChooser.chooseOpenDialog(this, "Import");

            if (projectFile != null) {
                // currentConfig.getHANtuneManager().closeProject();
                hanTune.currentConfig.getHANtuneManager().openProject(projectFile, true);
                hanTune.showProjectPanel(true);
            }
        }
    }


    private void flashTargetActionPerformed(java.awt.event.ActionEvent evt) {
        // Flash a file
        FlashCalibration flashSRec = new FlashCalibration(hanTune);
        flashSRec.chooseFilenameAndFlash();
    }

    private void readLogActionPerformed(java.awt.event.ActionEvent evt) {
        // read a logfile
        LogReaderAction logReaderAction = LogReaderAction.getInstance();
        logReaderAction.chooseLogFileAndRead();
    }


    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {
        hanTune.requestExitHANtune();
    }


    private void createNewDAQlist() {
        // ASAP2 file loaded?
        if (hanTune.getActiveAsap2Id() == -1) {
            MessagePane.showInfo("Please load an ASAP2 file first!");
            return;
        }

        // show popup to enter application
        String name = "";
        boolean validName = false;
        while (!validName) {
            name = HANtune.showQuestion("New DAQ list - Enter name", "Enter the name of the DAQ list:", "");
            if (name == null)
                break;
            validName = true;
            if (name.equals("")) {
                MessagePane.showError("Please enter a valid name");
                validName = false;
            }
            for (DAQList.DAQList DAQList : hanTune.daqLists) {
                if (DAQList.getName().equals(name)) {
                    MessagePane
                        .showError("A DAQlist with this name already exists\n please choose another name");
                    validName = false;
                }
            }
        }

        // perform add action
        if (name != null) {
            hanTune.currentConfig.getHANtuneManager().newDaqlist(name, true);
        }

    }

}
