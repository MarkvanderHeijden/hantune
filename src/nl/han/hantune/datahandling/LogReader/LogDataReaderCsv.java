/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.LogReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ErrorLogger.AppendToLogfile;
import nl.han.hantune.datahandling.LogReader.CsvLogInfo.ProtocolSignalName;
import util.OptimizedRandomAccessFile;

public class LogDataReaderCsv implements LogDataReader<CsvLogInfo, String, LogDataReaderCsv.ReaderResult> {
    // When parsing HANtune CSV files, they can contain ',' or '.' as decimal separator, therefore
    // replace by '.' to by sure.
    // It is assumed that HANtune CSV files do NOT contain a thousands separator
    static final char DECIMAL_SEPARATOR_COMMA = ',';
    static final char DECIMAL_SEPARATOR_DOT = '.';


    public class ReaderResult {
        double timeStamp;
        List<ResultItem> resultItems;

        ReaderResult(Double time, List<ResultItem> items) {
            timeStamp = time;
            resultItems = items;
        }

        public double getTimeStampSecs() {
            return timeStamp;
        }

        public List<ResultItem> getResultItems() {
            return resultItems;
        }
    }

    public class ResultItem {
        String protocolName;
        String itemName;
        double itemValue;

        ResultItem(String prot, String name, double val) {
            protocolName = prot;
            itemName = name;
            itemValue = val;
        }

        public String getProtocolName() {
            return protocolName;
        }

        public String getItemName() {
            return itemName;
        }

        public double getItemValue() {
            return itemValue;
        }

    }

    private static final int MAX_HEADER_LINES = 30; // don't look beyond this number of lines for header.
    private File readerFile;
    private OptimizedRandomAccessFile raf;

    private String errString = "";

    private LogTimes logTimes = new LogTimes();
    CsvLogInfo logHeaderInfo = new CsvLogInfo();


    public boolean init(File file) {
        readerFile = file;
        try {
            raf = new OptimizedRandomAccessFile(readerFile, "r");
            if (!readHeader(raf)) {
                return false;
            }
            if (!logTimes.readTimes(raf)) {
                return false;
            }

        } catch (IOException ex) {
            handleException(ex);
            return false;
        }
        return true;
    }


    public void terminateReader() {
        if (raf != null) {
            try {
                raf.close();
            } catch (IOException e) {
                AppendToLogfile.appendError(Thread.currentThread(), e);
            }
            raf = null;
        }
    }


    /**
     * Returns reader error message, if available.
     *
     * @return error message
     */
    public String getError() {
        return this.errString;
    }

    public double getMinimumLogTimeSec() {
        return logTimes.getMinTimeValue();
    }

    public double getMaximumLogTimeSec() {
        return logTimes.getMaxTimeValue();
    }

    public int getNumDataLines() {
        return logTimes.getTimeposSize();
    }

    public double getDelayToAdjacentStep(boolean direction){
        return logTimes.getDurationCurrentToAdjacentElement(direction);
    }

    private boolean readHeader(OptimizedRandomAccessFile raf) throws IOException {
        String line;
        int lineCount = 0;
        while ((line = raf.readLine()) != null && lineCount < MAX_HEADER_LINES) {
            lineCount++;
            if (line.length() <= 1) { // skip 'almost empty' lines
                continue;
            }

            if (logHeaderInfo.getProjectName() == null && parseProjectName(line)) {
                continue;
            }

            if (logHeaderInfo.getEcuId() == null && parseEcuId(line)) {
                continue;
            }

            if (parseDbcId(line)) {
                continue;
            }

            if (parseDaqListId(line)) {
                continue;
            }

            if (logHeaderInfo.getStartDate() == null && parseDate(line)) {
                continue;
            }

            if (parseProtocols(line)) {
                continue;
            }

            if (parseSignalNames(line) && logHeaderInfo.isHeaderComplete()) {
                return true;
            }
        }
        throw new IOException("Error parsing file header. Header incomplete at line " + lineCount);
    }


    private boolean parseProjectName(String line) {
        Pattern p = Pattern.compile("project: (.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find()) {
            logHeaderInfo.setProjectName(m.group(1));
            return true;
        }
        return false;
    }

    private boolean parseEcuId(String line) {
        Pattern p = Pattern.compile("ecu: (.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find()) {
            logHeaderInfo.setEcuId(m.group(1));
            return true;
        }
        return false;
    }

    private boolean parseDaqListId(String line) {
        Pattern p = Pattern.compile("DAQ List\\s*(.*):\\s*(.*),\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 3)) {

            logHeaderInfo.addDaqListInfo(m.group(1), m.group(2), m.group(3));
            return true;
        }
        return false;
    }

    private boolean parseDbcId(String line) {
        Pattern p = Pattern.compile("^Active DBC:\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 1)) {

            logHeaderInfo.addDbcInfo(m.group(1));
            return true;
        }
        return false;
    }


    private boolean parseDate(String line) {
        Pattern p = Pattern.compile("^Date:\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 1)) {

            logHeaderInfo.setStartDate(m.group(1));
            return true;
        }
        return false;
    }


    private boolean parseProtocols(String line) {
        Pattern p = Pattern.compile("^(;)\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 2)) {
            String[] names = m.group(2).split(";");
            logHeaderInfo.addProtocolNames(names);
            return true;
        }
        return false;
    }


    private boolean parseSignalNames(String line) throws IOException {
        // why the ':*' in pattern? Some old logfiles still have a leftover ':' behind 'Time'
        Pattern p = Pattern.compile("^(Time:*;)\\s*(.*)", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(line);
        if (m.find() && (m.groupCount() == 2)) {
            String[] names = m.group(2).split(";");
            logHeaderInfo.addSignalNames(names);
            return true;
        }
        return false;
    }


    @Override
    public CsvLogInfo getLogHeaderInfo() {
        return logHeaderInfo;
    }


    @Override
    public ReaderResult getFirstElement() throws IOException {
        try {
            raf.seek(logTimes.getFirstFilePosition());
            return convertResultLine(raf.readLine());
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }


    @Override
    public ReaderResult getLastElement() throws IOException {
        try {
            raf.seek(logTimes.getLastFilePosition());
            return convertResultLine(raf.readLine());
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }

    @Override
    public ReaderResult getNextElement() throws IOException {
        long pos = logTimes.getNextFilePosition();
        if (pos == LogTimes.OUT_OF_RANGE) {
            return null;
        } else {
            try {
                raf.seek(pos);
                return convertResultLine(raf.readLine());
            } catch (IOException ex) {
                handleException(ex);
                throw ex;
            }
        }
    }


    @Override
    public ReaderResult getPrevElement() throws IOException {
        long pos = logTimes.getPrevFilePosition();
        if (pos == LogTimes.OUT_OF_RANGE) {
            return null;
        } else {
            try {
                raf.seek(pos);
                return convertResultLine(raf.readLine());
            } catch (IOException ex) {
                handleException(ex);
                throw ex;
            }
        }
    }


    @Override
    public ReaderResult getRefElement(String ref) throws IOException {
        try {
            raf.seek(logTimes.getFilePositionAtGivenTime(ref));
            return convertResultLine(raf.readLine());
        } catch (IOException ex) {
            handleException(ex);
            throw new IOException(ex);
        }
    }


    /**
     * Set current reading location at refPos with given offset and direction
     *
     * @param refPos         string holding number of sec as a decimal value: xxx.yyy
     * @param offset         number of csv-lines from refPos
     * @param isBeyondRefPos true: offset beyond refPos, false: before refPos
     * @return number of csv-lines available from refPos (mostly same as offset), or OUT_OF_RANGE on error
     */
    public int gotoRefElementWithOffset(String refPos, int offset, boolean isBeyondRefPos) {
        return logTimes.moveFilePositionToGivenTimeAndOffset(refPos, isBeyondRefPos ? offset : -offset);
    }

    /**
     * Move current reading location with given number of lines
     * @param offset         number of csv-lines from current reading location
     * @param beyondCurrentPos true: offset beyond current pos, false: before current pos
     * @return number of csv-lines actually moved.
     */
    public int moveCurrentReadingLocation(int offset, boolean beyondCurrentPos){
        return logTimes.moveCurrentTimePosIdx(offset, beyondCurrentPos);
    }

    /**
     * Get element at relative position
     * @param relativeVal decimal value between 0 and 1 representing beginning - end
     * @return value at given relative position
     * @throws IOException
     */
    @Override
    public ReaderResult getRelativeElement(double relativeVal) throws IOException {
        try {
            raf.seek(logTimes.getFilePositionRelative(relativeVal));
            return convertResultLine(raf.readLine());
        } catch (IOException ex) {
            handleException(ex);
            throw ex;
        }
    }


    private ReaderResult convertResultLine(String line) throws IOException {
        List<ResultItem> items = new ArrayList<>();

        int idx = 0;
        try {
            // some HANtune .CSV files have a comma as decimal separator, therefore replace here
            String cLine = line.replace(DECIMAL_SEPARATOR_COMMA, DECIMAL_SEPARATOR_DOT);
            int startOfData = cLine.indexOf(';');
            Double timeVal = Double.parseDouble(cLine.substring(0, startOfData));
            String[] dataArr = cLine.substring(startOfData + 1).split(";");

            for (idx = 0; idx < dataArr.length; idx++) {
                if (!dataArr[idx].isEmpty()) {
                    ProtocolSignalName id = logHeaderInfo.getProtocolSignalNames().get(idx);
                    items.add(new ResultItem(id.getProtocol(), id.getName(), Double.parseDouble(dataArr[idx])));
                }
            }
            return new ReaderResult(timeVal, items);
        } catch (NumberFormatException ex) {
            throw new IOException("Error parsing value.  " + ex.getMessage(), ex);
        }
    }


    private void handleException(Exception ex) {
        errString = String.format("Filename: %s\n%s", readerFile.getName(), ex.getMessage());
        AppendToLogfile.appendMessage(errString);
        AppendToLogfile.appendError(Thread.currentThread(), ex);

        terminateReader();
    }
}
