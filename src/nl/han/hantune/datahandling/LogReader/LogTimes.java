/*
Copyright (c) 2020 [HAN University of Applied Sciences]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package nl.han.hantune.datahandling.LogReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import util.OptimizedRandomAccessFile;

public class LogTimes {
    private List<TimeFilePos> timePos = new ArrayList<>();
    private int currentTimePosIdx;
    static final int OUT_OF_RANGE = -1;

    private class TimeFilePos {
        private double timeVal;
        private long filePosition;

        TimeFilePos(double time, long pos) {
            timeVal = time;
            filePosition = pos;
        }

        double getTimeVal() {
            return timeVal;
        }

        public long getFilePosition() {
            return filePosition;
        }

    }


    private long getFilePositionAtIdx(int idx) {
        currentTimePosIdx = idx;
        return timePos.get(idx).filePosition;
    }


    public boolean readTimes(OptimizedRandomAccessFile raf) throws IOException {
        String line = "";
        long startOfDataLine = raf.getFilePointer();
        try {
            while ((line = raf.readLine()) != null) {
                int stopIdx = line.indexOf(';');
                if (stopIdx < 1) {
                    continue;
                }
                timePos.add(new TimeFilePos(Double.parseDouble(line.substring(0, stopIdx)
                    .replace(LogDataReaderCsv.DECIMAL_SEPARATOR_COMMA, LogDataReaderCsv.DECIMAL_SEPARATOR_DOT)),
                    startOfDataLine));
                startOfDataLine = raf.getFilePointer();
            }
        } catch (NumberFormatException e) {
            throw new IOException("Could not parse line containing: " + line.substring(0, 10), e);
        }

        return timePos.size() > 0;
    }


     long getFirstFilePosition() {
        return getFilePositionAtIdx(0);
    }


     long getLastFilePosition() {
        return getFilePositionAtIdx(timePos.size() - 1);
    }


     long getNextFilePosition() {
        int idx = currentTimePosIdx + 1;
        if (idx >= timePos.size()) {
            return OUT_OF_RANGE;
        }
        return getFilePositionAtIdx(idx);
    }


     long getPrevFilePosition() {
        int idx = currentTimePosIdx - 1;
        if (idx < 0) {
            return OUT_OF_RANGE;
        }
        return getFilePositionAtIdx(idx);
    }

    /**
     * Get absolute duration (in sec) between current and adjacent element
     * @param direction true/false: next/prev element
     * @return duration between 2 adjacent elements. 0.0 if no adjacent element in given direction.
     */
    double getDurationCurrentToAdjacentElement(boolean direction){
        if (direction) {
            int idx = currentTimePosIdx + 1;
            if (idx >= timePos.size()) {
                return 0.0d;
            }
            return timePos.get(idx).timeVal - timePos.get(currentTimePosIdx).timeVal;
        } else{
            int idx = currentTimePosIdx - 1;
            if (idx < 0) {
                return 0.0d;
            }
            return timePos.get(currentTimePosIdx).timeVal - timePos.get(idx).timeVal;
        }
    }


    /**
     * Move currentTimePosIdx with given offset
     * @param offset number of items to move
     * @return number of items index has been moved in given direction
     */
    int moveCurrentTimePosIdx(int offset, boolean forward) {
        int idx;
        int avail;
        if (forward) {
            idx = currentTimePosIdx + offset;
            if (idx >= timePos.size()) {
                idx = timePos.size() - 1;
            }
            avail = idx - currentTimePosIdx;
        } else {
            idx = currentTimePosIdx - offset;
            if (idx < 0) {
                idx = 0;
            }
            avail = currentTimePosIdx - idx;
        }
        currentTimePosIdx = idx;
        return avail;
    }


    double getMaxTimeValue() {
        return timePos.get(timePos.size() - 1).getTimeVal();
    }

    double getMinTimeValue() {
        return timePos.get(0).getTimeVal();
    }

    int getTimeposSize() {
        return timePos.size();
    }

    /**
     * file position relative to timePos size
     *
     * @param relVal must be between 0.0 and 1.0
     * @return timePos[0] when relVal <= 0.0 timePos[size -1] when relVal >= 1.0. else timePos[relVal * size]
     */
    public long getFilePositionRelative(double relVal) {
        int posIdx;
        if (relVal <= 0d) {
            posIdx = 0;
        } else if (relVal >= 1.0d) {
            posIdx = timePos.size() - 1;
        } else {
            posIdx = (int) (relVal * timePos.size());
        }

        return getFilePositionAtIdx(posIdx);
    }


    public long getFilePositionAtGivenTime(String givenTimeStr) {
        try {
            int endIdx = timePos.size() - 1;
            double givenTime = Double.parseDouble(givenTimeStr);
            double startTime = timePos.get(0).timeVal;
            double endTime = timePos.get(endIdx).timeVal;
            if (givenTime <= startTime) {
                return getFilePositionAtIdx(0);
            } else if (givenTime >= endTime) {
                return getFilePositionAtIdx(endIdx);
            }

            int estimatedIdx = (int) (((givenTime - startTime) / (endTime - startTime)) * (endIdx + 1));
            Double estimatedTime = timePos.get(estimatedIdx).timeVal;

            if (estimatedTime > givenTime) {
                return searchNearestFilePosition(givenTime, estimatedIdx, estimatedTime, endIdx, false);
            } else if (estimatedTime < givenTime) {
                return searchNearestFilePosition(givenTime, estimatedIdx, estimatedTime, endIdx, true);
            } else {
                return getFilePositionAtIdx(estimatedIdx);
            }
        } catch (NumberFormatException ex) {
            return OUT_OF_RANGE;
        }
    }


    public int moveFilePositionToGivenTimeAndOffset(String refPos, int offset) {
        if (getFilePositionAtGivenTime(refPos) != OUT_OF_RANGE) {
            int refPosIdx = currentTimePosIdx;
            int newIdx = currentTimePosIdx + offset;
            if (newIdx >= timePos.size()) {
                newIdx = timePos.size() - 1;
            } else if (newIdx < 0) {
                newIdx = 0;
            }
            currentTimePosIdx = newIdx;
            return offset > 0 ? newIdx - refPosIdx : refPosIdx - newIdx;
        }
        return OUT_OF_RANGE;
    }


    private long searchNearestFilePosition(double givenTime, int searchIdx, Double searchTime, int endIdx,
                                           boolean searchForward) {
        Double prevSearchTime;
        int prevSearchIdx;
        if (searchForward) {
            do {
                prevSearchTime = searchTime;
                prevSearchIdx = searchIdx;
                if (++searchIdx > endIdx) {
                    return getFilePositionAtIdx(endIdx);
                }
                searchTime = timePos.get(searchIdx).timeVal;
            } while (searchTime < givenTime);
        } else {
            do {
                prevSearchTime = searchTime;
                prevSearchIdx = searchIdx;
                if (--searchIdx < 0) {
                    return getFilePositionAtIdx(0);
                }
                searchTime = timePos.get(searchIdx).timeVal;
            } while (searchTime > givenTime);
        }
        int nearestIdx =
            (Math.abs(givenTime - searchTime) < Math.abs(givenTime - prevSearchTime)) ? searchIdx : prevSearchIdx;
        return getFilePositionAtIdx(nearestIdx);
    }

}
